using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public enum RotationAxis{X, Y, Z}
    
    public RotationAxis rotationAxis;
    public float speed;
    public bool counterClockwise;
    
    // Start is called before the first frame update
    void Start()
    {
    }
    

    // Update is called once per frame
    void Update()
    {
        int direction = 1;
        if (!counterClockwise)
        {
            direction = 1;
        }
        else
        {
            direction = -1;
        }
        switch(rotationAxis)
        {
            case RotationAxis.X: transform.Rotate( direction * Time.deltaTime * speed, 0, 0, Space.Self ); break;
            case RotationAxis.Y: transform.Rotate( 0, direction * Time.deltaTime * speed, 0, Space.Self ); break;
            case RotationAxis.Z: transform.Rotate( 0, 0, direction * Time.deltaTime * speed, Space.Self ); break;
        }
    }
}
